const mongoose = require("mongoose");

const postSchema = new mongoose.Schema({
    title:{
        type:String,
        required:true,
    },
    slug:{
        type:String,
        required:true,
        unique:true
    },
    description:{
        type:String,
        required:true,
    },
    publishDate:{
        type: Date,
        required:true,
        default: Date.now
    },
    thumbnail:{
        type:String,
        required:true,
    },
    categoryid:{
        type:mongoose.Types.ObjectId,
        ref:"category"
    }
})

module.exports = mongoose.model("blog",postSchema);