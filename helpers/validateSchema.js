const Joi = require('joi');


//loginSchema
const adminLoginValiateSchema = Joi.object({
    username:Joi.string()
                .required(),
    password:Joi.string()
                .required(),
});

//categorySchema
const categoryValidateSchema = Joi.object({
    name:Joi.string()
            .required()
});

//blog
const blogValidateSchema = Joi.object({
    title:Joi.string()
            .required(),
    description:Joi.string()
                    .required(),
    categoryid:Joi.string()
                  .required(),
    publishDate:Joi.number()
                    .required(),
});

module.exports = {
    adminLoginValiateSchema,
    categoryValidateSchema,
    blogValidateSchema
}