//imports
const jwt = require("jsonwebtoken");
require("dotenv").config();
const CustomException = require("../helpers/customError");

/**
 * middleware for Authenicate user for Views
 */
const viewAuth = (req,res,next) => {
    if(!req.cookies.token){
        return res.redirect("/admin/login")
    }
    let decodedToken;
    try{
        decodedToken = jwt.verify(req.cookies.token,process.env.JWTSECRET);
    }catch(err){
        return res.redirect("/admin/login")
    }

    if(!decodedToken){
        return res.redirect("/admin/login")
    }

    req.adminid = decodedToken.adminid
    next();
}


/**
 * middleware for Authenicate user for API
 */
const apiAuth = (req,res,next) => {
    try{
        const token = req.header("x-auth");
        if(!token){
            throw new CustomException("Not Authenticated","api",401)
        }

        let decodedToken;
        decodedToken = jwt.verify(token,process.env.JWTSECRET);
        

        if(!decodedToken){
            throw new CustomException("Not Authenticated","api",401)
            
        }

        req.adminid = decodedToken.adminid
        next();
    }catch(err){
        console.log(err.message);
        next(err);
    }
}

module.exports = {
    viewAuth,
    apiAuth
}