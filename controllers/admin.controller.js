//imports

//model
const Category = require("../models/Category.model");
const Blog = require("../models/Blog.model");


/**
 * @route GET admin/
 * @access private
 * @response will redirect to admin/blogs
 * @description Entry Route for admin 
 */
const getIndex = (req, res) => {
    res.redirect("/admin/blogs");
}


/**
 * @route GET admin/login
 * @access public
 * @response will render login Page From ejs
 * @description Login Page for Admin
 */
const getLogin = (req,res) => {

    res.render("admin/login");
}



/**
 * @route GET admin/blogs
 * @access private
 * @response will render Blogs Page From views folder
 * @description will display all blogs in website
 */
const getAllBlog = async (req,res,next) => {
    try{
        const blogs = await Blog.find().sort("-publishDate").populate("categoryid");

        res.render("admin/blogs",{
            blogs
        });
    }catch(err){
        console.log(err.message);
        err.type = "view";
        next(err)
    }
}


/**
 * @route GET admin/blogs/add
 * @access private
 * @response will render blog add Page From views folder
 * @description form will be display for adding new blog
 */
const getAddBlog = async (req,res) => {
    try {
        const categories = await Category.find();
        res.render("admin/blog-add",{
            categories,
            blog:null
        });
    } catch (err) {
        console.log(err.message);
        err.type = "view";
        next(err)
    }

}

/**
 * @route GET admin/blogs/edit/:id
 * @access private
 * @params ID
 * @response will render blog add Page From views folder
 * @description form will be display for Updating new blog
 */
const getEditBlog = async (req,res,next) => {
    const { id } = req.params;

    try {
        const blog = await Blog.findById(id);
        const categories = await Category.find();

        if(!blog)
            return res.render("404");

        
        res.render("admin/blog-add",{
            categories,
            blog
        });
    } catch (err) {
        console.log(err.message);
        err.type = "view";
        next(err)
    }

}


/**
 * @route GET admin/categories
 * @access private
 * @response will render categories page From views folder
 * @description will display all categories in website
 */
const getAllCategories = async (req,res,next) => {

    try{
        const categories = await Category.find();
        res.render("admin/categories",{
            categories
        });
    }catch(err){
        console.log(err.message);
        err.type = "view";
        next(err)
    }

}

/**
 * @route GET admin/categories/add
 * @access private
 * @response will render categories add Page From views folder
 * @description form will be display for adding new category
 */
const getAddCategories = (req,res) => {

    res.render("admin/category-add",{
        category:null
    });
}

/**
 * @route GET admin/categories/edit/:id
 * @access private
 * @params ID
 * @response will render categories add Page From views folder
 * @description form will be display for updating category
 */
const getEditCategories = async (req,res,next) => {
    const {id} = req.params;
    try{

        const category = await Category.findById(id);

        if(!category)
            return res.render("404");

        res.render("admin/category-add",{
            category
        });
    }catch(err){
        console.log(err.message);
        err.type = "view";
        next(err)
    }

}


//exports
module.exports = { 
    getIndex,
    getLogin,
    getAllBlog,
    getAddBlog,
    getEditBlog,
    getAllCategories,
    getAddCategories,
    getEditCategories
};