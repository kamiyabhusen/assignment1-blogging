//imports
const CustomException = require("../../helpers/customError");
const { categoryValidateSchema } = require("../../helpers/validateSchema");

//models
const Category = require("../../models/Category.model");


/**
 * @route POST api/categories
 * @access private
 * @body name
 * @response 200 with newly created category otherwise error
 * @description API Route for creating categories
 */
const postCategory = async (req,res,next) => {

    const { error } = categoryValidateSchema.validate(req.body);

    if(error)
        throw new CustomException(error,"api",400);

    const { name } = req.body;

    try{
        const category = new Category({name});

        await category.save();

        return res.status(200).json({data:category});
    }catch(err){
        console.log(err.message);
        next(err);
    }
}


/**
 * @route PUT api/categories
 * @access private
 * @params ID
 * @body name
 * @response 200 with updated category otherwise error
 * @description API Route for Upating categories
 */
const putCategory = async (req,res,next) => {

    const { error } = categoryValidateSchema.validate(req.body);

    if(error)
        throw new CustomException(error,"api",400);

    let { id } = req.params;
    const { name } = req.body;
    try{
        let category = await Category.findByIdAndUpdate(id,{
            name
         });

        return res.status(200).json({data:category});
    }catch(err){
        console.log(err.message);
        next(err);
    }
}


/**
 * @route DELETE api/categories
 * @access private
 * @params ID
 * @response 200 with deleted category otherwise error
 * @description API Route for deleting categories
 */
const deleteCategory = async (req,res,next) => {
    let { id } = req.params;
    try{
        let category = await Category.findByIdAndDelete(id,req.body);

        return res.status(200).json({data:category});
    }catch(err){
        console.log(err.message);
        next(err);
    }
}

//exports
module.exports = { 
    postCategory,
    putCategory,
    deleteCategory,
};