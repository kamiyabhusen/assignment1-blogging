//imports
const jwt = require("jsonwebtoken");
require("dotenv").config();
const CustomException = require("../../helpers/customError");
const { adminLoginValiateSchema } = require("../../helpers/validateSchema");

//model
const Admin = require("../../models/Admin.model");


/**
 * @route POST api/admin/login
 * @access public
 * @response jwt token on Success otherwise Error
 * @description API Route for admin Login
 */
const postLogin = async (req,res,next) => {
    try{

        const { error } = adminLoginValiateSchema.validate(req.body);

        if(error)
        throw new CustomException(error,"api",400);

        //fetch admin by username
        let admin = await Admin.findOne({username:req.body.username});

        //check veriable is null or not if null then username is incorrent
        if(!admin)
            throw new CustomException("Username is Incorrect","api",400);


        //checking password
        if(admin.password !== req.body.password)
            throw new CustomException("Password is Incorrect","api",400);

        //creating JWT token
        const token = jwt.sign({
            adminid: admin._id
          }, process.env.JWTSECRET, { expiresIn: '1h' });

        return res.status(200).json({token});

    }catch(err){
        if(!err.status)
            console.log(err.message);
        
        next(err);
    }
    
}

module.exports = { 
    postLogin,
};