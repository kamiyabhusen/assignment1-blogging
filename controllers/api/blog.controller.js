//imports
const fs = require("fs");
const CustomException = require("../../helpers/customError");
const slugify = require("../../helpers/slugify");
const { blogValidateSchema } = require("../../helpers/validateSchema");

//models
const Blog = require("../../models/Blog.model");


/**
 * @route POST api/blogs
 * @access private
 * @body Title, Description, publishDate, Thumbnail, Category
 * @response 200 with newly created blog otherwise error
 * @description API Route for creating blog
 */
const postBlog = async (req,res,next) => {
    try {

        const { error } = blogValidateSchema.validate(req.body);

        if(error)
            throw new CustomException(error,"api",400);

        if(!req.file)
            throw new CustomException("Please Select File or Select Correct File Type (PNG,JPG,JPEG)","api",406);

        const { filename:thumbnail } = req.file;
        const {title,categoryid,description,publishDate} = req.body;

        const blog = new Blog({
            title,
            slug:slugify(title),
            description,
            publishDate,
            thumbnail,
            categoryid
        });

        await blog.save();

        return res.json({data:blog});
        
    } catch (err) {
        console.log(err.message);
        err.type="api";
        next(err);
    }


}


/**
 * @route PUT api/blogs
 * @access private
 * @body Title, Description, publishDate, Thumbnail, Category
 * @response 200 with newly created blog otherwise error
 * @description API Route for creating blog
 */
const putBlog = async (req,res,next) => {


    let { id } = req.params;
    
    const { error } = blogValidateSchema.validate(req.body);

    if(error)
        throw new CustomException(error,"api",400);
    
    const {title,categoryid,description,publishDate} = req.body;
    let thumbnail;
    
    if(req.file){
        thumbnail = req.file.filename;
    }else{
        thumbnail = req.body.Thumbnail;
    }
    
    
    try{
        let blog = await Blog.findByIdAndUpdate(id,{
            title,
            description,
            publishDate,
            thumbnail,
            categoryid
         });

        return res.status(200).json({data:blog});
    }catch(err){
        console.log(err.message);
        err.type="api";
        next(err);
    }

}


/**
 * @route DELETE api/blog
 * @access private
 * @params ID
 * @response 200 with deleted blog otherwise error
 * @description API Route for deleting blog
 */
const deleteBlog = async (req,res,next) => {
    let { id } = req.params;
    try{
        let blog = await Blog.findByIdAndDelete(id);

        fs.unlink('./public/uploads/'+blog.thumbnail,function(err){
            if(err) throw err
        });

        return res.status(200).json({data:blog});
    }catch(err){
        console.log(err.message);
        err.type="api";
        next(err);
    }
}


//exports
module.exports = { 
    postBlog,
    putBlog,
    deleteBlog
};