const CustomException = require("../helpers/customError");

//model
const Category = require("../models/Category.model");
const Blog = require("../models/Blog.model");

/**
 * @route GET /
 * @access public
 * @response will render index page
 * @description Entry Route for guest user 
 */
const getIndex = async (req, res,next) => {

    try {
        let numberOfBlog = 2;
        let { page } = req.query;

        if(!page)
            page = 1;

        let blogs = Blog.find()
                        .sort("-publishDate");

        //get length of blogs
        let {length:blogsLength} = await blogs;

        //limit the result for pagination
        blogs = await blogs.skip((page-1)*numberOfBlog)
                            .limit(numberOfBlog);

        const categories = await Category.find();

        let next = true;
        if(blogsLength <= (page * numberOfBlog))
            next = false;

        res.render("blog/index",{
            page,
            next,
            title:"Home",
            blogs,
            categories
        });

    } catch (error) {
        console.log(error.message);
        error.type = "view";
        next(error)
    }

}


/**
 * @route GET /:slug
 * @access public
 * @response will render single post page
 * @description single post by slug
 */
const getSinglePost = async (req, res,next) => {

    const {slug} = req.params;

    try {
        const blog = await Blog.findOne({slug}).populate("categoryid");
        const categories = await Category.find();

        if(!blog)
            throw new CustomException("not found","view",404);

        return res.render("blog/single",{
            title:blog.title,
            blog,
            categories
        });
    } catch (error) {
        if(!error.status)
            console.log(err.message);
        error.type = "view";
        next(error)
    }

}

/**
 * @route GET /search
 * @access public
 * @response will render search page
 * @description query result page from search page
 */
const getSearchQuery = async (req, res,next) => {
    
    try {

        let { q, page } = req.query;
        let numberOfBlog = 2;

        if(!page)
            page = 1;

        let blogs = Blog.find({title:{$regex:''+q+'',$options: 'i'}})
                        .sort("-publishDate");

        let {length:blogsLength} = await blogs;

        blogs = await blogs.skip((page-1)*numberOfBlog)
                            .limit(numberOfBlog);

        const categories = await Category.find();

        let next = true;
        if(blogsLength <= (page * numberOfBlog))
            next = false;
        
        res.render("blog/search",{
            page,
            next,
            title:`search result for '${q}'`,
            q,
            blogs,
            categories
        });

    } catch (error) {
        console.log(error.message);
        error.type = "view";
        next(error)
    }

}

module.exports = {
    getIndex,
    getSinglePost,
    getSearchQuery
}