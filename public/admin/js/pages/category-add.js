(function($) {
    "use strict";
    
    $("#category-add-form").on("submit",async function(e){
        e.preventDefault();

        let name = $("#inputName").val().trim();

        const response = await fetch("/api/categories",{
            
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-auth':localStorage.getItem("token")
            },
            method: 'POST',
            body: JSON.stringify({name})
        });

        const data = await response.json();

        if(data.err){
            $("#alert").attr("class","alert alert-danger");
            $("#alert").html(data.err);
            return
        }

        window.location = "/admin/categories"
    });

    $("#category-edit-form").on("submit",async function(e){
        e.preventDefault();

        let name = $("#inputName").val().trim();
        let id = $("#catid").val().trim();

        const response = await fetch("/api/categories/"+id,{
            
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-auth':localStorage.getItem("token")
            },
            method: 'PUT',
            body: JSON.stringify({name})
        });

        const data = await response.json();

        if(data.err){
            $("#alert").attr("class","alert alert-danger");
            $("#alert").html(data.err);
            return
        }

        window.location = "/admin/categories"
    });

})(jQuery);