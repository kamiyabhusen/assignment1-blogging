(function($) {
    "use strict";
    
    $("#loginForm").on("submit",async function(e){
        e.preventDefault();

        let username = $("#inputUsername").val().trim();
        let password = $("#inputPassword").val().trim();
        
        if(username == ""){
            $("#alert").attr("class","alert alert-danger");
            $("#alert").html("Please Enter Username");
            return
        }
        if(password == ""){
            $("#alert").attr("class","alert alert-danger");
            $("#alert").html("Please Enter password");
            return
        }

        const response = await fetch("/api/admin/login",{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({username,password})
        });

        const data = await response.json();

        if(data.err){
            $("#alert").attr("class","alert alert-danger");
            $("#alert").html(data.err);
            return
        }

        document.cookie = "token="+data.token;
        localStorage.setItem("token", data.token);

        window.location = "/admin/"
    });

})(jQuery);