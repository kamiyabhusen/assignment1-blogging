var quill = new Quill('#editor', {
    theme: 'snow'
  });

  (function($) {
    "use strict";
    
    $("#blog-add-form").on("submit",async function(e){
        e.preventDefault();

        let title = $("#inputTitle").val().trim();
        let categoryid = $("#inputCategory").val();
        let description = quill.root.innerHTML;
        let publishDate = $("#inputPublishDate").val();
        let Thumbnail = document.querySelector('#inputThumbnail').files[0];

        publishDate =  new Date(publishDate).getTime();

        const formData = new FormData();
        formData.append("title",title);
        formData.append("categoryid",categoryid);
        formData.append("description",description);
        formData.append("publishDate",publishDate);
        formData.append("Thumbnail",Thumbnail);



        const response = await fetch("/api/blogs",{
            
            headers: {
                'x-auth':localStorage.getItem("token")
            },
            method: 'POST',
            body: formData
        });

        const data = await response.json();
        console.log(data);
        if(data.err){
            $("#alert").attr("class","alert alert-danger");
            $("#alert").html(data.err);
            return
        }

        window.location = "/admin/blogs"
    });

    $("#blog-edit-form").on("submit",async function(e){
        e.preventDefault();

        let id = $("#blogid").val().trim();
        let title = $("#inputTitle").val().trim();
        let categoryid = $("#inputCategory").val();
        let description = quill.root.innerHTML;
        let publishDate = $("#inputPublishDate").val();
        let Thumbnail = document.querySelector('#inputThumbnail').files[0];
        if(!Thumbnail){
            Thumbnail = $("#thumbnailtext").val();
        }

        publishDate =  new Date(publishDate).getTime();

        const formData = new FormData();
        formData.append("title",title);
        formData.append("categoryid",categoryid);
        formData.append("description",description);
        formData.append("publishDate",publishDate);
        formData.append("Thumbnail",Thumbnail);



        const response = await fetch("/api/blogs/"+id,{
            
            headers: {
                'x-auth':localStorage.getItem("token")
            },
            method: 'PUT',
            body: formData
        });

        const data = await response.json();
        console.log(data);
        if(data.err){
            $("#alert").attr("class","alert alert-danger");
            $("#alert").html(data.err);
            return
        }

        window.location = "/admin/blogs"
    });

})(jQuery);