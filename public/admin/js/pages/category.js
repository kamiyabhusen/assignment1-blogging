(function($) {
    "use strict";
    
    $(document).on("click",".delete", function(e){
        let id = $(this).data("id");
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(async (willDelete) => {
            if (willDelete) {
              
                const response = await fetch("/api/categories/"+id,{
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'x-auth':localStorage.getItem("token")
                    },
                });
        
                const data = await response.json();
        
                window.location = "/admin/categories"

            }
          });
        e.preventDefault();

    
        
    });

})(jQuery);