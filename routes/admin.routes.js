const express = require("express");
const router = express.Router();

//controller
const adminController = require("../controllers/admin.controller")

//middleware
const { viewAuth } = require("../middlewares/auth")

router.get("/",viewAuth,adminController.getIndex);
router.get("/login",adminController.getLogin);

router.get("/blogs", viewAuth, adminController.getAllBlog);
router.get("/blogs/add", viewAuth, adminController.getAddBlog);
router.get("/blogs/edit/:id",viewAuth,adminController.getEditBlog);


router.get("/categories",viewAuth,adminController.getAllCategories);
router.get("/categories/add",viewAuth,adminController.getAddCategories);
router.get("/categories/edit/:id",viewAuth,adminController.getEditCategories);


module.exports = router;