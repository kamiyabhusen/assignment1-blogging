const express = require("express");
const router = express.Router();

//controllers
const apiAdminController = require("../controllers/api/admin.controller");
const apiCategoryController = require("../controllers/api/category.controller");
const apiBlogController = require("../controllers/api/blog.controller");

//middleware
const { apiAuth } = require("../middlewares/auth");
const mullterMiddleware = require("../helpers/multerConfig");

//Admin auth
router.post("/admin/login",apiAdminController.postLogin);


//API Routes for Categories
router.post("/categories",apiAuth,apiCategoryController.postCategory);
router.put("/categories/:id",apiAuth,apiCategoryController.putCategory);
router.delete("/categories/:id",apiAuth,apiCategoryController.deleteCategory);


//API Routes for Blogs
router.post("/blogs",[apiAuth, mullterMiddleware.single("Thumbnail")],apiBlogController.postBlog);
router.put("/blogs/:id",[apiAuth, mullterMiddleware.single("Thumbnail")],apiBlogController.putBlog);
router.delete("/blogs/:id",apiAuth,apiBlogController.deleteBlog);

module.exports = router;