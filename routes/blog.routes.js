const express = require("express");
const router = express.Router();

//controller
const blogController = require("../controllers/blog.controller")


router.get("/",blogController.getIndex);
router.get("/search",blogController.getSearchQuery);
router.get("/:slug",blogController.getSinglePost);


module.exports = router;