//imports
const express = require("express");
const path = require("path");
const cookieParser = require('cookie-parser')

//config
require("./config/db");



const app = express();
const PORT = process.env.PORT || 5000;

//set views
app.set("view engine","ejs");
app.set("views","views");

//static folder
app.use(express.static(path.join(__dirname, 'public')));

//middleware
app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(cookieParser());



//routes
app.use("/admin",require("./routes/admin.routes"));
app.use("/api",require("./routes/api.routes"));
app.use("/",require("./routes/blog.routes"));

//404 page
app.use((req,res,next) => {
    res.render("404")
})

//error Handling
app.use((err, req, res, next) => {
    res.status(err.status || 500)

    if(err.type == "api"){
        if(!err.status)
            return res.json({err:"Internal Server Error"});

        return res.json({err:err.message})  
    }else{
        if(err.status == 404)
            return res.render("404")
        
        res.render("500")
    }
    
});

app.listen(PORT,() => {
    console.log("app Started");
});
